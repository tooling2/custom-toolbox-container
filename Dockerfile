ARG INSTALL_GIT_ON_RUNTIME=false

ARG UID=1000

FROM binbash/leverage-toolbox:1.3.5-0.1.13 as base

MAINTAINER  <juan.delacamara@binbash.co>


ARG ASDF_VER=v0.14.0


ENV DEBIAN_FRONTEND=noninteractive
ENV ASDF_VER=$ASDF_VER
ENV ASDF_DIR=/opt/asdf
ENV ASDF_BIN=$ASDF_DIR/bin/asdf

RUN mkdir /opt/home
ENV HOME=/opt/home

RUN apt update && \
    apt upgrade -y

RUN apt install -y \
    git \
    curl \
    unzip

RUN git clone https://github.com/asdf-vm/asdf.git $ASDF_DIR --branch $ASDF_VER

RUN echo ". ${ASDF_DIR}/asdf.sh" >> ${HOME}/.bashrc
RUN echo ". ${ASDF_DIR}/completions/asdf.bash" >> ${HOME}/.bashrc

COPY ./extra-opts/ /opt

# Run extra scripts for base
COPY extra-scripts/ ${HOME}/extra-scripts
RUN for s in $(ls ${HOME}/extra-scripts/base_*); do /bin/bash $s; done

# ###########################################################################

FROM binbash/leverage-toolbox:1.3.5-0.1.13 as runtime

RUN apt update && apt install -y \
    curl \
    jq \
    less \
    sudo \
    bash-completion \
    make \
    podman \
    buildah

ARG INSTALL_GIT_ON_RUNTIME
ENV INSTALL_GIT_ON_RUNTIME=${INSTALL_GIT_ON_RUNTIME}
RUN if [ "$INSTALL_GIT_ON_RUNTIME" = "true" ]; \
    then \
        echo "Installing GIT" ; \
        apt install -y git ; \
    else \
        echo "Not installing GIT" ; \
    fi

ENV ASDF_DIR=/opt/asdf
ENV ASDF_BIN=$ASDF_DIR/bin/asdf
ENV HOME=/opt/home

COPY --from=base /opt /opt

# add the asdf plugins list to file asdf_plugins_list.env
COPY asdf_install_plugins.sh .
COPY asdf_plugins_list.env .
RUN bash asdf_install_plugins.sh

# bash completion
RUN echo ". /usr/share/bash-completion/bash_completion" >> ${HOME}/.bashrc

# aliases
COPY bash_aliases ${HOME}
RUN cat ${HOME}/bash_aliases >> ${HOME}/.bashrc

COPY init.sh ${HOME}
RUN chmod +x ${HOME}/init.sh
COPY add_user.sh ${HOME}
RUN chmod +x ${HOME}/add_user.sh
COPY MESSAGE ${HOME}

# Run extra scripts for runtime
COPY extra-scripts/ ${HOME}/extra-scripts
RUN for s in $(ls ${HOME}/extra-scripts/runtime_*); do /bin/bash $s; done

CMD ["/bin/bash", "-c", "/opt/home/init.sh && /bin/bash --rcfile ${HOME}/.bashrc"]

# Extra for BinbashLeverage scripts
RUN mv /root/scripts ${HOME} && \
    find ${HOME}/scripts -type f -exec sed -E 's|/root/|'${HOME}'/|' {} -i \;

# ###########################################################################

FROM runtime

ARG UID
ARG USER=toolbox

RUN /opt/home/add_user.sh && \
    chown ${USER}:${USER} -R /opt
USER ${USER}
