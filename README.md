# Juan Kungfoo Toolbox

## Build

``` shell
docker build --build-arg INSTALL_GIT_ON_RUNTIME=true -t juanmatias/toolbox:dev-0.0.1 .
# or 
buildah bud --layers --build-arg INSTALL_GIT_ON_RUNTIME=true -t juanmatias/toolbox:dev-0.0.1 

```

## Run it

``` shell
podman run --rm -it ${DOCKER_SOCKET} -e AWSCLI_DEFAULT=2.11.15 -e TERRAFORM_DEFAULT=1.3.5 -e KUBECTL_DEFAULT=1.29.2 -v ${HOME}/.aws/:/opt/home/.aws -v ${HOME}/.kube/:/opt/home/.kube ${TOOLBOX_NAME}:${TOOLBOX_VER}
```

## Add more `asdf` plugins

Just add the plugins and versions to the `asdf_plugins_list.env` file:

``` shell
#!/usr/bin/env zsh

# ASDF_PLUGINS is a list like this
# plugin-name:versions;plugin2-name:versions
# versions is a comma sepparated list with versions, the default one is prepended by * (if no * the last one is the default)
# e.g.
# ASDF_PLUGINS=kubectl:*1.23.4,1.26.14,1.29.2;terraform:1.3.5,1.5.0,1.6.0
ASDF_PLUGINS="kubectl:*1.23.4,1.26.14,1.29.2;terraform:1.3.5,1.5.0,1.6.0;awscli:2.11.15;k9s:0.32.4"

```
