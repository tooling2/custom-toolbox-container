#!/usr/bin/env sh

# set the plugins to install in this env file
source ./asdf_plugins_list.env

for pin in $(echo ${ASDF_PLUGINS} | tr ';' ' ');
do
    plugindata=($(echo ${pin} | tr ':' ' '))
    pluginname=${plugindata[0]}
    pluginversions=${plugindata[1]}
    echo Working with plugin ${pluginname}
    echo Versions ${pluginversions}

    echo Installing plugin
    CMD="${ASDF_DIR}/bin/asdf plugin add ${pluginname}" && \
    echo $CMD && \
    eval $CMD

    defaultversion=""
    lastversion=""
    for ver in $(echo ${pluginversions} | tr ',' ' ');
    do
        if (echo ${ver} | grep -q "^\*");
        then
            echo "    Default version!"
            ver=$(echo ${ver} | sed -E 's/^\*//')
            defaultversion=${ver}
        fi
        echo "    installing version ${ver}"
        lastversion=${ver}

        ${ASDF_BIN} install ${pluginname} ${ver}

    done
    if [ "${defaultversion}" = "" ];
    then
        defaultversion=${lastversion}
    fi
    echo "Setting default version to ${defaultversion}"
    ${ASDF_BIN} global ${pluginname} ${defaultversion}
done
