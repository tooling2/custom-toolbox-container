.PHONY: help
##
# Project Title
#
# @file
# @version 0.1

MAKEFILE_LIST := "./Makefile"
help:
	@echo 'Available Commands:'
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf " - \033[36m%-18s\033[0m %s\n", $$1, $$2}'

build-local: ## Build local
	buildah bud --layers --build-arg INSTALL_GIT_ON_RUNTIME=true -t juanmatias/toolbox:dev-0.0.1 .

build-push: ## Build and push
	echo buildah bud --layers --build-arg INSTALL_GIT_ON_RUNTIME=true -t juanmatias/toolbox:dev-0.0.1

run: ## Run the image
	@./command.sh
# end
