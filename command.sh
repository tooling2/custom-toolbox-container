#!/usr/bin/env sh

export TOOLBOX_NAME=juanmatias/toolbox
export TOOLBOX_VER=dev-0.0.1
export MOUNT_DOCKER_SOCKET=false

if [ "${MOUNT_DOCKER_SOCKET}" = "true" ];
then
    DOCKER_SOCKET="-v /var/run/docker.sock:/var/run/docker.sock"
fi

podman run --rm --privileged -it ${DOCKER_SOCKET} -e AWSCLI_DEFAULT=2.11.15 -e TERRAFORM_DEFAULT=1.3.5 -e KUBECTL_DEFAULT=1.29.2  -v ${HOME}/.aws/:/opt/home/.aws -v ${HOME}/.kube/:/opt/home/.kube ${TOOLBOX_NAME}:${TOOLBOX_VER}
