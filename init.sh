#!/bin/bash

cat ${HOME}/MESSAGE

if [ ! "${KUBECTL_DEFAULT}" = "" ];
then
    echo "Setting kubectl to ${KUBECTL_DEFAULT}"
    ${ASDF_BIN} global kubectl ${KUBECTL_DEFAULT}
fi
if [ ! "${TERRAFORM_DEFAULT}" = "" ];
then
    echo "Setting terraform to ${TERRAFORM_DEFAULT}"
    ${ASDF_BIN} global terraform ${TERRAFORM_DEFAULT}
fi
if [ ! "${AWSCLI_DEFAULT}" = "" ];
then
    echo "Setting awscli to ${AWSCLI_DEFAULT}"
    ${ASDF_BIN} global awscli ${AWSCLI_DEFAULT}
fi

$ASDF_BIN list
