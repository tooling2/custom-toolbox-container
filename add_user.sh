#!/usr/bin/env sh

# Check if the user with UID=1000 exists
if getent passwd ${UID} &>/dev/null; then
    # If the user exists, delete it
    sudo userdel -r -f "$(getent passwd ${UID} | cut -d: -f1)"
    echo "User with UID=${UID} deleted."
else
    echo "User with UID=${UID} does not exist."
fi

useradd --system ${USER} --home ${HOME} -u ${UID}
echo "${USER} ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# to run podman and buildah
usermod --add-subuids 10000-75535 ${USER}
usermod --add-subgids 10000-75535 ${USER}
