alias k=kubectl
alias kcg="kubectl config get-contexts | sed -E 's/^[^\*] +/- /' | awk  '{print \$1, \$2}'"
alias kcu='kubectl config use-context'
alias kd='kubectl describe'
alias kdp='kubectl describe po'
alias ke='kubectl exec -it'
alias kg='kubectl get'
alias kgcm='kubectl get cm'
alias kgi='kubectl get ingress'
alias kgn='kubectl get ns'
alias kgp='kubectl get po'
alias kgs='kubectl get secret'
alias kgsvc='kubectl get svc'
alias kl='kubectl logs -f'
